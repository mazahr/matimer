#include <gtkmm.h>
#include <iostream>
#include <sstream>

template <typename T>
std::string toStr(const T& t, std::ios_base& (*f)(std::ios_base&)=std::dec) throw(bool){
    std::stringstream truc;
    truc.precision(1);
    if((truc<<f<<t).fail()) throw true;
    return truc.str();
}

GTimer *cas=g_timer_new();
int h, m, s;
sigc::connection con;
class Okno : public Gtk::Window {
        public:
                Okno();
                void klik();
                void klik2();
                void klik3();
                bool check();
        protected:
                Glib::RefPtr<Gtk::Adjustment> ad_hod, ad_min, ad_sec;
                Gtk::SpinButton hod, min, sec;
		Gtk::Button ok,stop,go;
		Gtk::Label vystup,l1,l2,l3;
		Gtk::VBox Vram,v1,v2,v3;
		Gtk::HBox Hram,h1;
};
Okno::Okno()
: vystup("initializing"), l1("Hodiny"), l2("Minuty"), l3("Sekundy"), ad_hod(Gtk::Adjustment::create(0.0, 0.0, 10.0, 1.0, 2.0, 0.0)), ad_min(Gtk::Adjustment::create(0.0, 0.0, 59.0, 1.0, 5.0, 0.0)), ad_sec(Gtk::Adjustment::create(0.0, 0.0, 59.0, 1.0, 5.0, 0.0)), hod(ad_hod), min(ad_min), sec(ad_sec)
{
        ok.signal_clicked().connect(sigc::mem_fun(*this, &Okno::klik));
        stop.signal_clicked().connect(sigc::mem_fun(*this, &Okno::klik2));
        go.signal_clicked().connect(sigc::mem_fun(*this, &Okno::klik3));
        vystup.set_markup("<span font=\"80\">0 . 00 : 00.0</span>");
        hod.set_numeric(true);
        min.set_numeric(true);
        sec.set_numeric(true);
        set_border_width(20);
        add(Vram);
        Vram.pack_start(Hram);
        Hram.pack_start(v1);
        Hram.pack_start(v2);
        Hram.pack_start(v3);
        v1.pack_start(l1);
        v1.pack_start(l2);
        v1.pack_start(l3);
        v2.pack_start(hod);
        v2.pack_start(min);
        v2.pack_start(sec);
        v3.pack_start(ok);
        v3.pack_start(h1);
        h1.pack_start(stop);
        h1.pack_start(go);
        Vram.pack_start(vystup);
        ok.set_label("Spustit");
        stop.set_label("Pozastavit");
        go.set_label("Pustit");
        show_all_children();
}
void Okno::klik(){g_timer_start(cas); h=hod.get_value(); m=min.get_value(); s=sec.get_value();
        sigc::slot<bool> slotm = sigc::mem_fun(*this, &Okno::check);
        con = Glib::signal_timeout().connect(slotm, 10);
}
void Okno::klik2(){
	g_timer_stop(cas);
	con.disconnect();
}
void Okno::klik3(){
        sigc::slot<bool> slotm = sigc::mem_fun(*this, &Okno::check);
	g_timer_continue(cas);
        con = Glib::signal_timeout().connect(slotm, 10);
}
bool Okno::check(){
        int hv, mv;
        double sv;
        double elapsed=g_timer_elapsed(cas,NULL);
        sv=(int(elapsed+s)%60)+(elapsed-int(elapsed));
        elapsed=int(elapsed+s)/60;
        mv=int(elapsed+m)%60;
        elapsed=int(elapsed+m)/60;
        hv=int(elapsed+h);
        std::ostringstream str;
        vystup.set_markup("<span font=\"80\">"+toStr(hv)+" . "+toStr(mv)+" : "+toStr(sv,std::fixed)+"</span>");
        return true;
}
int main(int argc, char *argv[]){
        Gtk::Main kit(argc, argv);
        Okno okno;
        Gtk::Main::run(okno);
        return 0;
}
